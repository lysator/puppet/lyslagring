class lyslagring::nfsclient {
  package { [ 'autofs', 'nfs-utils' ]:
    ensure => installed,
  }
  -> file { '/etc/auto.master':
    ensure  => file,
    name    => '/etc/auto.master',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => '/home	yp:auto_home	nosuid,nodev,sec=sys,sec=krb5:krb5i:krb5p',
  }
  -> service { 'autofs':
    ensure    => running,
    enable    => true,
    subscribe => File['/etc/auto.master'],
  }
}
