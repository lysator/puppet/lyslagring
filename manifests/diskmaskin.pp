#Installs Ceph
class lyslagring::diskmaskin {
  require ::lyslagring::hosts
  require ::lyslagring::ceph_vogon
  $last_octet = split($facts['networking']['ip'], '\.')[3]

  ipoib::interface { 'ibp130s0':
    ipaddress => "10.44.1.${last_octet}",
    netmask   => '255.255.0.0',
  }

  package { 'ceph-common':
    ensure => latest,
  }
  ~> file {
    '/etc/ceph/client.diskmaskin-home-backup.secret':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0400',
      source => 'puppet:///modules/lyslagring/diskmaskin-home-secret',
  }

  ~> file_line { 'mount ceph':
    ensure => present,
    path   => '/etc/fstab',
    line   => '10.44.1.99:6789:/   /ceph-home  ceph    name=diskmaskin,mds_namespace=home,secretfile=/etc/ceph/client.diskmaskin-home-backup.secret,noatime,_netdev,ro   0   2',
    match  => '^10\.44\.1\.99',
  }

}
