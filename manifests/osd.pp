#Configures a host to be an OSD-node for the Ceph cluster. Only networking at the moment.
class lyslagring::osd {
  require ::lyslagring::hosts
  require ::lyslagring::ceph_vogon
  require ::lyslagring::common

  $last_octet = split($facts['networking']['ip'], '\.')[3]

  ipoib::interface { 'ib0':
    ipaddress => "10.44.1.${last_octet}",
    netmask   => '255.255.0.0',
  }

  ipoib::interface { 'ib1':
    ipaddress => "10.43.1.${last_octet}",
    netmask   => '255.255.255.0',
  }
}
