#Configures a node in the Ceph cluster for use as a monitor node.
class lyslagring::mon {
  require ::lyslagring::hosts
  require ::lyslagring::ceph_vogon
  require ::lyslagring::common

  $last_octet = split($facts['networking']['ip'], '\.')[3]

  ipoib::interface { 'ib1':
    ipaddress => "10.44.1.${last_octet}",
    netmask   => '255.255.0.0',
  }

}
