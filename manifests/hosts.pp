#Ensures that the file /etc/hosts is the same across the Ceph cluster. Except for the first line which is the local host.
class lyslagring::hosts {
  require ::lysnetwork::hosts
  concat::fragment { '/etc/hosts/01-lyslagring':
    target => '/etc/hosts',
    source => 'puppet:///modules/lyslagring/hosts';
  }
}
