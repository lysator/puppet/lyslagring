class lyslagring::nfsserver {
  require ::lyslagring::hosts
  require ::lyslagring::ceph_vogon
  require ::lyslagring::common

  Package { ensure => installed }

  $packages = [
    'nfs-ganesha-rados-grace',
    'libcephfs2',
    'nfs-ganesha-ceph',
    'nfs-ganesha',
    'pacemaker',
    'pcs',
    'resource-agents',
    'fence-agents-ipmilan',
  ]

  file {
    '/etc/yum.repos.d/nfs-ganesha-ceph.repo':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/lyslagring/ganesha-repo',
  }

  ~> package { $packages: }

  ~> file {
    '/etc/ceph/ceph.client.ganesha.keyring':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0400',
      source => 'puppet:///modules/lyslagring/ceph.client.ganesha.keyring',
  }

  ~> file {
    '/etc/ganesha/ganesha.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/lyslagring/ganesha-home-conf',
  }

}
