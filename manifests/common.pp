#Installs Ceph of a specific version
class lyslagring::common {

  Package { ensure => installed }

  $packages = [
    'ceph',
  ]

  file {
    '/etc/yum.repos.d/ceph.repo':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/lyslagring/repo',
  }

  ~> package { $packages: }

}
